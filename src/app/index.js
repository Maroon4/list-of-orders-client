import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import {NavBar} from '../components'
import {
    OrdersList,
    OrdersInsert,
    OrdersUpdate,
    OrderPage
} from '../pages'

import 'bootstrap/dist/css/bootstrap.min.css'

function App() {
    return (
        <Router>
            <NavBar />
            <Switch>
                <Route path="/orderspage" exact component={OrderPage}/>
                <Route path="/orders" exact component={OrdersList} />
                <Route path="/orders/create" exact component={OrdersInsert} />
                <Route
                    path="/orders/update/:id"
                    exact
                    component={OrdersUpdate}
                />
            </Switch>
        </Router>
    )
}

export default App