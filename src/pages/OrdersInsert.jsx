import React, { Component } from 'react'
import {Redirect} from 'react-router-dom'
import api from '../api'

import styled from 'styled-components'

const Title = styled.h1.attrs({
    className: 'h1',
})``;

const Wrapper = styled.div.attrs({
    className: 'form-group',
})`
    margin: 0 30px;
`;

const Label = styled.label`
    margin: 5px;
`;

const InputText = styled.input.attrs({
    className: 'form-control',
})`
    margin: 5px;
`;

const Button = styled.button.attrs({
    className: `btn btn-primary`,
})`
    margin: 15px 15px 15px 5px;
`;

const CancelButton = styled.a.attrs({
    className: `btn btn-danger`,
})`
    margin: 15px 15px 15px 5px;
`;

class OrdersInsert extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user_email: '',
            date: new Date(),
            value: '',
            currency: '',
            status: '',
            orderCreated: false
        }
    }

    handleChangeInputUserEmail = async event => {
        const user_email = event.target.value;
        this.setState({ user_email })
    };

    // handleChangeInputDate = async event => {
    //     const date = event.target.validity.valid
    //         ? event.target.value
    //         : this.state.date;
    //
    //     this.setState({ date })
    // };

    handleChangeInputValue = async event => {
        const value = event.target.value;
        this.setState({ value })
    };

    handleChangeInputCurrency = async event => {
        const currency = event.target.value;
        this.setState({ currency })
    };

    handleChangeInputStatus = async event => {
        const status = event.target.value;
        this.setState({ status })
    };

    handleIncludeOrder = async () => {

        const {
            user_email,
            date,
            value, currency, status } = this.state;
        // const arrayTime = time.split('/')
        const payload = { user_email,
            date,
            value, currency, status };

        this.setState({
            user_email: '',
            date: '',
            value: '',
            currency: '',
            status: '',
            orderCreated: true
        });

        console.log(this.state.orderCreated);

        await api.insertOrder(payload).then(res => {
            // window.alert(`Order inserted successfully`);

            // this.setState({
            //     user_email: '',
            //     date: '',
            //     value: '',
            //     currency: '',
            //     status: '',
            //     orderCreated: true
            // })
        })
    };

    render() {
        const { user_email,
            // date,
            value, currency, status  } = this.state;

        if (this.state.orderCreated === true) {
            return <Redirect to="/orders"/>
        }

        return (
            <Wrapper>
                <Title>Create Order</Title>

                <Label>User Email: </Label>
                <InputText
                    type="text"
                    value={user_email}
                    onChange={this.handleChangeInputUserEmail}
                />

                {/*<Label>Date: </Label>*/}
                {/*<InputText*/}
                {/*    type="number"*/}
                {/*    // step="0.1"*/}
                {/*    // lang="en-US"*/}
                {/*    // min="0"*/}
                {/*    // max="10"*/}
                {/*    // pattern="[0-9]+([,\.][0-9]+)?"*/}
                {/*    value={date}*/}
                {/*    onChange={this.handleChangeInputDate}*/}
                {/*/>*/}

                <Label>Value: </Label>
                <InputText
                    type="text"
                    value={value}
                    onChange={this.handleChangeInputValue}
                />

                <Label>Currency: </Label>
                <InputText
                    type="text"
                    value={currency}
                    onChange={this.handleChangeInputCurrency}
                />

                <Label>Status: </Label>
                <InputText
                    type="text"
                    value={status}
                    onChange={this.handleChangeInputStatus}
                />

                <Button onClick={this.handleIncludeOrder}>Add Order</Button>
                <CancelButton href={'/orders'}>Cancel</CancelButton>
            </Wrapper>
        )
    }
}

export default OrdersInsert