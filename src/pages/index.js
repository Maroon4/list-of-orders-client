import OrdersInsert from "./OrdersInsert";
import OrdersList from "./OrdersList";
import OrdersUpdate from "./OrdersUpdate";
import OrderPage from "./OrdersPage";

export {
    OrdersInsert,
    OrdersList,
    OrdersUpdate,
    OrderPage
}