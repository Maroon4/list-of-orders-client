import React, { Component } from 'react'
import ReactTable from 'react-table-6'
import api from '../api'

import styled from 'styled-components'

import 'react-table-6/react-table.css'

const Wrapper = styled.div`
    padding: 0 40px 40px 40px;
`;

const Update = styled.div`
    color: #ef9b0f;
    cursor: pointer;
`;

const Delete = styled.div`
    color: #ff0000;
    cursor: pointer;
`;

class UpdateOrder extends Component {
    updateUser = event => {
        event.preventDefault();

        window.location.href = `/orders/update/${this.props.id}`
    };

    render() {
        return <Update onClick={this.updateUser}>Update</Update>
    }
}

class DeleteOrder extends Component {
    deleteUser = event => {
        event.preventDefault();

        if (
            window.confirm(
                `Do tou want to delete the order ${this.props.id} permanently?`,
            )
        ) {
            api.deleteOrderById(this.props.id);
            window.location.reload()
        }
    };

    render() {
        return <Delete onClick={this.deleteUser}>Delete</Delete>
    }
}


class OrdersList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            orders: [],
            columns: [],
            isLoading: false,
        }
    }

    componentDidMount = async () => {
        this.setState({ isLoading: true });

        await api.getAllOrders().then(orders => {
            this.setState({
                orders: orders.data.data,
                isLoading: false,
            })
        })
    };

    render() {
        const { orders, isLoading } = this.state;
        console.log('TCL: OrdersList -> render -> orders', orders);

        const columns = [
            // {
            //     Header: 'ID',
            //     accessor: '_id',
            //     filterable: true,
            // },
            {
                Header: 'User email',
                accessor: 'user_email',
                filterable: true,
            },
            {
                Header: 'Date',
                accessor: 'date',
                filterable: true,
            },
            {
                Header: 'Value',
                accessor: 'value',
                filterable: true,
            },
            {
                Header: 'Currency',
                accessor: 'currency',
                // Cell: props => <span>{props.value.join(' / ')}</span>,
            },
            {
                Header: 'Status',
                accessor: 'status',
                filterable: true,
            },

            {
                Header: '',
                accessor: '',
                Cell: function(props) {
                    return (
                        <span>
                            <DeleteOrder id={props.original._id} />
                        </span>
                    )
                },
            },
            {
                Header: '',
                accessor: '',
                Cell: function(props) {
                    return (
                        <span>
                            <UpdateOrder id={props.original._id} />
                        </span>
                    )
                },
            },

        ];

        let showTable = true;
        if (!orders.length) {
            showTable = false
        }

        return (
            <Wrapper>
                {showTable && (
                    <ReactTable
                        data={orders}
                        columns={columns}
                        loading={isLoading}
                        defaultPageSize={10}
                        showPageSizeOptions={true}
                        minRows={0}
                    />
                )}
            </Wrapper>
        )
    }
}

export default OrdersList