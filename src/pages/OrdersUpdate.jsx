import React, { Component } from 'react'
import {Redirect} from 'react-router-dom'
import api from '../api'

import styled from 'styled-components'

const Title = styled.h1.attrs({
    className: 'h1',
})``;

const Wrapper = styled.div.attrs({
    className: 'form-group',
})`
    margin: 0 30px;
`;

const Label = styled.label`
    margin: 5px;
`;

const InputText = styled.input.attrs({
    className: 'form-control',
})`
    margin: 5px;
`;

const Button = styled.button.attrs({
    className: `btn btn-primary`,
})`
    margin: 15px 15px 15px 5px;
`;

const CancelButton = styled.a.attrs({
    className: `btn btn-danger`,
})`
    margin: 15px 15px 15px 5px;
`;

class OrdersUpdate extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.match.params.id,
            user_email: '',
            date: '',
            value: '',
            currency: '',
            status: '',
            orderCreated: false
        }
    }

    handleChangeInputUserEmail = async event => {
        const user_email = event.target.value;
        this.setState({ user_email })
    };

    // handleChangeInputDate = async event => {
    //     const date = event.target.validity.valid
    //         ? event.target.value
    //         : this.state.date;
    //
    //     this.setState({ date })
    // };


    handleChangeInputValue = async event => {
        const value = event.target.value;
        this.setState({ value })
    };

    handleChangeInputCurrency = async event => {
        const currency = event.target.value;
        this.setState({ currency })
    };

    handleChangeInputStatus = async event => {
        const status = event.target.value;
        this.setState({ status })
    };



    handleUpdateOrder = async () => {
        const { id, user_email,
            date,
            value, currency, status  } = this.state;
        // const arrayTime = time.split('/')
        const payload = { user_email,
            date,
            value, currency, status  };

        console.log(this.state);


            this.setState({
                user_email: '',
                date: '',
                value: '',
                currency: '',
                status: '',
                orderCreated: true
            });


        await api.updateOrderById(id, payload).then(res => {
            window.alert(`Order updated successfully`);

        })
    };

    componentDidMount = async () => {
        const { id } = this.state;
        const order = await api.getOrderById(id);

        // console.log(order);

        this.setState({
            user_email: order.data.data.user_email,
            date: order.data.data.date,
            value: order.data.data.value,
            currency: order.data.data.currency,
            status: order.data.data.status

        })
    };

    render() {
        const { user_email, value, currency, status  } = this.state;

       if (this.state.orderCreated === true) {
           return <Redirect to="/orders"/>
       }

        return (
            <Wrapper>
                <Title>Update Order</Title>

                <Label>User Email: </Label>
                <InputText
                    type="text"
                    value={user_email}
                    onChange={this.handleChangeInputUserEmail}
                />

                <Label>Value </Label>
                <InputText
                    type="text"
                    value={value}
                    onChange={this.handleChangeInputValue}
                />

                <Label>Currency </Label>
                <InputText
                    type="text"
                    value={currency}
                    onChange={this.handleChangeInputCurrency}
                />

                <Label>Status </Label>
                <InputText
                    type="text"
                    value={status}
                    onChange={this.handleChangeInputStatus}
                />

                <Button onClick={this.handleUpdateOrder}>Update Order</Button>
                <CancelButton href={'/orders'}>Cancel</CancelButton>
            </Wrapper>
        )
    }
}

export default OrdersUpdate