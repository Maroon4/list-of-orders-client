import React from 'react';
import { Link } from 'react-router-dom';

import api from '../api';
import DownloadCsv from "../components/DownloadCsv";
import OrderCsvReader from "../components/CSVReader";
import DownloadCsvReport from "../components/DownloadCsvReport";

class OrderPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            pager: {},
            pageOfOrders: []
        };
    }

    componentDidMount() {
        this.loadPage();
    }

    componentDidUpdate() {
        this.loadPage();
    }

    async loadPage() {
        // get page of items from api
        const params = new URLSearchParams(window.location.search);

        const page = parseInt(params.get('page')) || 1;
        // const page = 1;
        console.log(page)
        if (page !== this.state.pager.currentPage) {
           await api.getOrderPage(page)
                .then(res => res.data)
                .then(({pageOfOrders, pager}) => {
                    this.setState({ pageOfOrders, pager });
                });
           // console.log(res)
        }
    }

    render() {
        const { pager, pageOfOrders } = this.state;



        console.log(pageOfOrders)

        return (
            <div className="card text-center m-3">
                <h3 className="card-header">Order List</h3>
                <div className="row card-columns">
                    <div className="col-2">USER EMAIL</div>
                    <div className="col-2">DATE</div>
                    <div className="col-1">VALUE</div>
                    <div className="col-1">CURRENCY</div>
                    <div className="col-1">STATUS</div>
                    <div className="col-2">Download CSV</div>
                    <div className="col-3">Upload order</div>
                </div>
                <div className="card-body row">
                    {pageOfOrders.map(item =>

                        <div key={item._id} className="col-9 row">
                            <div className="col-2 " >{item.user_email}</div>
                            <div className="col-3">{item.date}</div>
                            <div className="col-2">{item.value}</div>
                            <div className="col-1">{item.currency}</div>
                            <div className="col-2">{item.status}</div>
                            <div className="col-2">
                                <DownloadCsv id={item._id} />
                            </div>
                        </div>
                    )}
                    <div className="col-2"

                    >
                        <OrderCsvReader/>
                    </div>
                </div>
                <div className="card-footer pb-0 pt-3">
                    <div>
                        <DownloadCsvReport/>
                    </div>
                    {pager.pages && pager.pages.length &&
                    <ul className="pagination">
                        <li className={`page-item first-item ${pager.currentPage === 1 ? 'disabled' : ''}`}>
                            <Link to={{ search: `?page=1` }} className="page-link">First</Link>
                        </li>
                        <li className={`page-item previous-item ${pager.currentPage === 1 ? 'disabled' : ''}`}>
                            <Link to={{ search: `?page=${pager.currentPage - 1}` }} className="page-link">Previous</Link>
                        </li>
                        {pager.pages.map(page =>
                            <li key={page} className={`page-item number-item ${pager.currentPage === page ? 'active' : ''}`}>
                                <Link to={{ search: `?page=${page}` }} className="page-link">{page}</Link>
                            </li>
                        )}
                        <li className={`page-item next-item ${pager.currentPage === pager.totalPages ? 'disabled' : ''}`}>
                            <Link to={{ search: `?page=${pager.currentPage + 1}` }} className="page-link">Next</Link>
                        </li>
                        <li className={`page-item last-item ${pager.currentPage === pager.totalPages ? 'disabled' : ''}`}>
                            <Link to={{ search: `?page=${pager.totalPages}` }} className="page-link">Last</Link>
                        </li>
                    </ul>
                    }
                </div>
            </div>
        );
    }
}

export default OrderPage