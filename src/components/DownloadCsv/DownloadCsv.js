import React, {Component} from "react";
import { CSVLink } from "react-csv";
import api from "../../api";

class DownloadCsv extends Component {

    state = {

        headers:  [
            { label: "User_email", key: "user_email" },
            { label: "Date", key: "date" },
            { label: "Value", key: "value" },
            { label: "Currency", key: "currency" },
            { label: "Status", key: "status" }
        ],

        data: [
          {
              user_email: '',
              date: '',
              value: '',
              currency: '',
              status: ''
          },

      ]

    };

    getData =  async ()  => {
       const id =  this.props.id;

       console.log(id);
       const order = await api.getOrderById(id);
       console.log(order.data.data);

      if (!order) {
          return alert("Error")
      } else {
          await this.setState({
              data:[
                  // [order.data.data]
                  {
                      user_email: order.data.data.user_email,
                      date: order.data.data.date,
                      value: order.data.data.value,
                      currency: order.data.data.currency,
                      status: order.data.data.status
                  },

              ]
          })
      }


    };

    componentDidMount() {
        this.getData()
    }

    render() {

        const data = this.state.data;
        const headers = this.state.headers;

        console.log(data);


            return (
                <CSVLink
                    data={data}
                    headers={headers}
                    // asyncOnClick={true}
                    // onClick={() => {
                    //     this.getData();
                    //     // console.log(this.state.data)
                    // }
                    // }

                    // onClick={(event, done) => {
                    //     axios.post("/spy/user").then(() => {
                    //         done(false); // Don't Proceed
                    //     });
                    // }}
                >
                    Download
                </CSVLink>
            );







    }
}

export default DownloadCsv;