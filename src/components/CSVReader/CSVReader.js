import React, { Component } from 'react'

import CSVReader from 'react-csv-reader'
import api from "../../api";

import "./styles.css"

// const handleForce = (data, fileName) => console.log(data, fileName);

// const papaparseOptions = {
//     header: true,
//     dynamicTyping: true,
//     skipEmptyLines: true,
//     transformHeader: header => header.toLowerCase().replace(/\W/g, "_")
// };

class OrderCsvReader extends Component{

    state = {
        payload: null
    };

    handleForce = async (data, fileName) => {


        const payload = data;

        // console.log(payload);

        this.setState({
            payload: payload
        })

        console.log(this.state.payload);
    };

    // sendCSVData = () => {
    //     api.insertOrder(this.state.payload).then(res => {
    //         // window.alert(`Order inserted successfully`);
    //
    //         console.log(this.state.payload)
    //
    //     })
    // };

    sendCSVData = () => {
        api.insertOrdersFromCSV(this.state.payload).then(res => {
            // window.alert(`Order inserted successfully`);

            console.log(this.state.payload)

        })
    };

    papaparseOptions = {
        header: true,
        dynamicTyping: true,
        skipEmptyLines: true,
        transformHeader: header => header.toLowerCase().replace(/\W/g, "_")
    };



    render() {
        return (
           <div>
               <CSVReader
                   cssClass="react-csv-input"
                   label="Select CSV with order"
                   onFileLoaded={this.handleForce}
                   parserOptions={this.papaparseOptions}
               />
               <button className="button-upload btn btn-success"
                   onClick={this.sendCSVData}
               >Upload
               </button>
           </div>
        )
    }
}

export default OrderCsvReader;