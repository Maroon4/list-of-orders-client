import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

const Collapse = styled.div.attrs({
    className: 'collpase navbar-collapse',
})``;

const List = styled.div.attrs({
    className: 'navbar-nav mr-auto',
})``;

const Item = styled.div.attrs({
    className: 'collpase navbar-collapse',
})``;

class Links extends Component {
    render() {
        return (
            <React.Fragment>
                <Link to="/" className="navbar-brand">
                    List of orders
                </Link>
                <Collapse>
                    <List>
                        <Item>
                            <Link to="/orderspage" className="nav-link">
                                Orders Page
                            </Link>
                        </Item>
                        <Item>
                            <Link to="/orders" className="nav-link">
                                Orders
                            </Link>
                        </Item>
                        <Item>
                            <Link to="orders/create" className="nav-link">
                                Create Order
                            </Link>
                        </Item>
                    </List>
                </Collapse>
            </React.Fragment>
        )
    }
}

export default Links