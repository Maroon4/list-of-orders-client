import axios from 'axios'
import dotenv from 'dotenv'
// const dotenv = require('dotenv');

const api = axios.create({
    // baseURL: 'https://list-of-orders.herokuapp.com/api',
    // baseURL: process.env.BEACKEND_URL
    baseURL:'http://localhost:3000/api'
});

export const insertOrder = payload => api.post(`/order`, payload);
export const insertOrdersFromCSV = payload => api.post('/order/csv', payload);
export const getAllOrders = () => api.get(`/orders`);
export const updateOrderById = (id, payload) => api.put(`/order/${id}`, payload);
export const deleteOrderById = id => api.delete(`/order/${id}`);
export const getOrderById = id => api.get(`/order/${id}`);
export const getOrderPage = (page) => api.get(`/home?page=${page}`);

export const getOrderInCsvById = id => api.get(`/order/download/${id}`);


const apis = {
    insertOrder,
    getAllOrders,
    updateOrderById,
    deleteOrderById,
    getOrderById,
    getOrderPage,
    insertOrdersFromCSV
};

export default apis